// /* Bootstrap Grid System */
// importing using deconstruction
import {Row, Col, Card, Button, CardGroup, Modal} from 'react-bootstrap'
import {Link} from 'react-router-dom';
import React, {useContext, useEffect, useState} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function Dashboard(){
    const {user} = useContext(UserContext);
    //const [productData, setProductData] = useState([]);
    const userToken = localStorage.getItem(`token`)
    const cardStyle = {
		transitionDuration: "0.1s",
		minHeight: "100%",
		maxHeight: 500,
	  };

    // useEffect(() => {
    //     fetch(`http://localhost:4000/products/allProducts`, {
    //         headers: {
    //             'Content-Type': 'application/json',
    //             Authorization : `Bearer ${localStorage.getItem(`token`)}`
    //         }
    //     }).then(response => response.json())
    //     .then(data =>{
    //         setProductData(data.map(product => {
    //             return (<Cards key={product._id} product={product}/>)
    //         }))
    //     })
    // },[])



	return(
        <Row><i><h2>My ~ Coffee</h2></i>
        <Col className="col-lg-4 col-md-6 col-12 mt-4">
			<CardGroup>
				<Card style={cardStyle} className="cardHighlight blog-preview">
				<Card.Img variant="top" alt = "example" src="https://www.myweekly.co.uk/wp-content/uploads/sites/9/2018/06/iStock-814684194-coffee-x.jpg" />
					<Card.Body>
						<Card.Title>Name</Card.Title>
							<Card.Text>Description</Card.Text>
					</Card.Body>
					<div className='mb-2'>
						{
							(userToken === null) 
							? <Link to = "/login"><Button onClick = {console.log(`here`)} variant="primary">More info...</Button></Link>

							: <Button onClick = {console.log(`here`)} variant="primary">More info...</Button>
						}
					</div>
				</Card>
			</CardGroup>
		</Col>
        </Row>
	)
}