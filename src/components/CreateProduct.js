import { useState, useEffect, useContext } from 'react';
import { Container, Col, Row } from 'react-bootstrap'

import UserContext from '../UserContext'
import { useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2'

export default function CreateProduct() {

  const [name, setName] = useState('');
  const [description, setDescription] = useState('')
  const [price, setPrice] = useState(0)
  const [quantity, setStock] = useState(0)

  const [isActive, setIsActive]  = useState(false);

  const { user } = useContext(UserContext)

  const navigate = useNavigate();


  useEffect(()=> {
    if(name !== '' && description !== '' && price !== 0 && quantity !== 0){
        setIsActive(true);
    }else{
      setIsActive(false);
    }

  }, [name, description, price, quantity])


  function createProduct (event){
    event.preventDefault()

    console.log(user)

    if(user.isAdmin){
      fetch(`${process.env.REACT_APP_URI}/products/createProduct`, {
        method: 'POST',
        headers: {
          'Content-Type' : 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          name,
          description,
          price, 
          quantity
          
        })
      })
      .then(response => response.json())
      .then(data => {
        console.log(data)

          Swal.fire({
            title: "Product Created",
            icon: 'success',
            text: 'Posted product!'
          })
           navigate('/');
        
      })
    }
    
  }



  return (
    <div>
        <Container className='mt-4'>
          <Row>
            <Col className='col-md-6 offset-md-3'>

              <form onSubmit={createProduct}>
              <input type='text' className='form-control mb-4 p-2' placeholder="Enter product name" value={name} onChange={(e) => setName(e.target.value)} required />

              <input type='text' className='form-control mb-4 p-2' placeholder="Description" value={description} onChange={(e) => setDescription(e.target.value)} required />

              <input type='text' className='form-control mb-4 p-2' placeholder="Price" value={price} onChange={(e) => setPrice(e.target.value)} required />

              <input type='text' className='form-control mb-4 p-2' placeholder="Stock" value={quantity} onChange={(e) => setStock(e.target.value)} required />

              <button className='btn btn-success' type='submit' disabled={!isActive}>Create Product</button>
              </form>
            </Col>
          </Row>
        </Container>
    </div>
  );
}
